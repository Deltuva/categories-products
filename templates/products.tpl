<ul>
{if count($products) > 0}
    {foreach from=$products item=rows}
        <li>#{$rows->getProductId()} {$rows->getName()} {$rows->getPrice()} (PVM {$rows->getPricePVM()})</li>
    {/foreach}
{else}
	<li>Products not found.</li>
{/if}
</ul>
