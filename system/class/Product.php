<?php

class Product
{

    public $product_id;
    public $category_id;
    public $product_name;
    public $product_price;
    public $product_pvm;

    public function __construct($product_id,$product_name,$product_price,$product_pvm){

        $this->prod_id    = $product_id;
        $this->prod_name  = $product_name;
        $this->prod_price = $product_price;
        $this->prod_pvm   = $product_pvm;

    }

    public function setCategoryId($category_id)
    {
        $this->category_id = $category_id;
    }
    public function setPrice($product_price)
    {
        $this->prod_price = $product_price;
    }
    public function setProductName($name)
    {
        $this->product_name = $name;
    }

    public function getProductId(){
        return $this->prod_id;
    }

    public function getName(){
        return $this->prod_name;
    }

    public function getPrice(){
        return $this->prod_price;
    }

    public function getPricePVM(){
        return $this->prod_pvm;
    }

}