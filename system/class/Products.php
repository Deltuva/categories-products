<?php

class Products
{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getProducts($params)
    {
        global $PVM;

        try {
            $products = $this->db->prepare("
                        SELECT product.product_id,product.category_id,product.product_name,
                        product.price,format(product.price-(ROUND(product.price*100/(100+{$PVM}),2)),2) AS pvm
                        FROM product
                        LEFT JOIN category ON product.category_id = category.category_id
                        WHERE product.category_id = '{$params->catid}'
                    ");
            $products->bindParam(':category_id', $params->catid, PDO::PARAM_INT);
            if ($products->execute(array($params->catid))) {
                $array = array();
                while ($rowsObj = $products->fetchObject()) {
                    $array[] = $this->getProduct($rowsObj->product_id,$rowsObj->product_name,$rowsObj->price,$rowsObj->pvm);
                }
                return $array;
            } else {
                echo 'Cannot execute query.';
            }
        } catch (PDOException $e) {
            throw new Exception('Invalid query');
            exit;
        }
    }

    public function getProduct($product_id,$product_name,$product_price,$product_price_pvm){
        return new Product(
            $product_id,
            $product_name,
            $product_price,
            $product_price_pvm);
    }

}