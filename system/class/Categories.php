<?php

class Categories
{
    public $db;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function getCategories()
    {
        try {
            $categories = $this->db->prepare("
					SELECT category.category_id,
					category.category_name,
                    SUM(
                        CASE
                           WHEN product.category_id IS NOT NULL THEN 1 ELSE 0
                        END) AS categories_sum
                    FROM category
                    LEFT JOIN product ON category.category_id = product.category_id
					GROUP BY category.category_id
				");
            if($categories->execute()){
                $array = array();
                while ($rowsObj = $categories->fetchObject()) {
                    $array[] = $this->getCategory($rowsObj->category_id,$rowsObj->category_name,$rowsObj->categories_sum);
                }
                return $array;
            } else {
                echo 'Cannot execute query.';
            }
        } catch (PDOException $e) {
            throw new Exception('Invalid query');
            exit;
        }
    }

    public function getCategory($category_id,$category_name,$categories_sum){
        return new Category(
            $category_id,
            $category_name,
            $categories_sum);
    }

}